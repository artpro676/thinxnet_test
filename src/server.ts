import App from "./app";

( async function () {
    try {

        const port = process.env.PORT || 3000;
        const isDebugMode = process.env.NODE_ENV !== 'production';

        const app = new App();

        const terminate = async ( signal: NodeJS.Signals ) => {
            await app.terminate( signal );
            process.exit( 0 );
        };

        process.on( "SIGINT", terminate );
        process.on( "SIGTERM", terminate );

        process.on( "uncaughtException", ( error: Error ) => {
            console.error( "Uncaught Exception:", error );
        } );
        process.on( "unhandledRejection", ( error: any, promise: Promise<any> ) => {
            console.error( "Unhandled promise rejection.", "Error: ", error, "Promise: ", promise );
        } );

        await app.start( +port, isDebugMode );
    } catch ( err ) {
        console.error( "TypeORM connection error: ", err );
    }

}() );

