
export class ApiError extends Error {
    statusCode: number;

    constructor(message: string, statusCode = 500) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }

}
