import {MigrationInterface, QueryRunner} from "typeorm";
import { DB_TABLE_NAME_ENUM } from "../entity/constants";

export class CreateSupportAgentTable1620217195123 implements MigrationInterface {

    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
CREATE TABLE \`${DB_TABLE_NAME_ENUM.SUPPORT_AGENT}\` (
	\`id\` INT(11) NOT NULL AUTO_INCREMENT,
	\`name\` VARCHAR(80) NOT NULL,
	\`email\` VARCHAR(100) NOT NULL,
	PRIMARY KEY (\`id\`),
	UNIQUE INDEX \`emil_indx\` (\`email\`)
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
;
`);
        await queryRunner.query(`INSERT INTO \`${DB_TABLE_NAME_ENUM.SUPPORT_AGENT}\` VALUES 
(NULL, 'agent1', 'agent1@mail.com'),
(NULL, 'agent2', 'agent2@mail.com'),
(NULL, 'agent3', 'agent3@mail.com')
;`);

    }

    async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`${DB_TABLE_NAME_ENUM.SUPPORT_AGENT}\`;`); // reverts things made in "up" method
    }
}
