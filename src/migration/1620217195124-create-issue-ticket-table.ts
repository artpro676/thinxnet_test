import {MigrationInterface, QueryRunner} from "typeorm";
import { DB_TABLE_NAME_ENUM } from "../entity/constants";

export class CreateIssueTicketTable1620217195124 implements MigrationInterface {

    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
CREATE TABLE \`${DB_TABLE_NAME_ENUM.ISSUE_TICKET}\` (
	\`id\` INT(11) NOT NULL AUTO_INCREMENT,
	\`description\` VARCHAR(255) NOT NULL,
	\`from\` VARCHAR(50) NOT NULL,
	\`status\` ENUM('PENDING','DONE') NOT NULL DEFAULT 'PENDING',
	\`created_at\` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	\`updated_at\` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	\`support_agent_id\` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (\`id\`),
	INDEX \`FK_issues_ticket_support_agent\` (\`support_agent_id\`),
	CONSTRAINT \`FK_issues_ticket_support_agent\` FOREIGN KEY (\`support_agent_id\`) REFERENCES \`support_agent\` (\`id\`) ON UPDATE CASCADE ON DELETE SET NULL
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
;
`);

    }

    async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`${DB_TABLE_NAME_ENUM.SUPPORT_AGENT}\`;`); // reverts things made in "up" method
    }
}
