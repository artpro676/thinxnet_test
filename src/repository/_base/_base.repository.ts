import { Repository, FindManyOptions, DeleteResult } from "typeorm";
import { UpdateResult } from "typeorm/query-builder/result/UpdateResult";
import { IRepository } from "./interfaces";

export class BaseRepository<T> implements IRepository<T>{

    protected provider: Repository<T>;

    constructor( provider: Repository<T> ) {
        this.provider = provider;
    }

    getOneById( id: number ) {
        return this.provider.findOne( id );
    }

    getList( options?: FindManyOptions ): Promise<T[]> {
        return this.provider.find( options );
    }

    create( data: Partial<T> ): Promise<T> {
        return this.provider.save( data as any )
    }

    deleteById( id: number ): Promise<DeleteResult> {
        return this.provider.delete( id );
    }

    updateById( id: number, patchData: Partial<T> ):Promise<UpdateResult> {
        return this.provider.update( id, patchData as any );
    }
}
