import { DeleteResult, FindManyOptions } from "typeorm";
import { UpdateResult } from "typeorm/query-builder/result/UpdateResult";

export interface IRepository<T> {
    getOneById( id: number ): Promise<T>;

    getList( options?: FindManyOptions ): Promise<T[]>;

    create( data: Partial<T> ) : Promise<T>;

    deleteById( id: number ): Promise<DeleteResult>;

    updateById( id: number, patchData: Partial<T> ): Promise<UpdateResult>;
}
