import { IssueTicketStatusEnum, SupportAgentEntity } from "../../entity";
import { BaseRepository } from "../_base";
import { IRepository } from "..";

export interface ISupportAgentRepository extends IRepository<SupportAgentEntity>{
    getFreeAgents(): Promise<SupportAgentEntity[]>;
}

export class SupportAgentRepository extends BaseRepository<SupportAgentEntity> implements ISupportAgentRepository {

    /**
     * Returns only agents which don`t have active tickets
     */
    getFreeAgents(){
        return this
            .provider
            .createQueryBuilder('agent')
            .leftJoinAndSelect( 'agent.tickets', 'tickets', `tickets.status = '${IssueTicketStatusEnum.PENDING}'`)
            .where('tickets.id IS NULL')
            .getMany();
    }

}
