import { IssueTicketEntity, IssueTicketStatusEnum } from "../../entity";
import { BaseRepository } from "../_base";
import { IRepository } from "..";

export interface IissueTicketRepository  extends IRepository<IssueTicketEntity> {
    getUnassignedTickets(): Promise<IssueTicketEntity[]>;
}

export class IssueTicketRepository extends BaseRepository<IssueTicketEntity> implements IissueTicketRepository{

    getUnassignedTickets() {
        return this.getList( { where: { supportAgentId: null } } )
    }
}
