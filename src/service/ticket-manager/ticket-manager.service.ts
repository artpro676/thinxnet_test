import autobind from "autobind-decorator";
import { EventEmitter } from "events";

import { IssueTicketEntity, SupportAgentEntity } from "../../entity";
import { IissueTicketRepository, ISupportAgentRepository } from "../../repository";


export enum TicketManagerServiceEventsEnum {
    ASSIGN_TICKETS = "ASSIGN_TICKETS"
}

export interface ITicketManagerServiceDependencies {
    supportAgentRepository: ISupportAgentRepository;
    issueTicketRepository: IissueTicketRepository;
}

@autobind
export class TicketManagerService {

    protected dependencies: ITicketManagerServiceDependencies;
    private eventEmitter: EventEmitter;

    constructor( dependencies: ITicketManagerServiceDependencies ) {
        this.dependencies = dependencies;

        this.eventEmitter = new EventEmitter();
        this.eventEmitter.on( TicketManagerServiceEventsEnum.ASSIGN_TICKETS, this.onAssigningTickets );
    }

    emitAssigningTickets() {
        return this.eventEmitter.emit( TicketManagerServiceEventsEnum.ASSIGN_TICKETS );
    }

    public updateTicket(ticket: IssueTicketEntity, ) {
        return this.dependencies.issueTicketRepository.updateById( ticket.id, ticket );
    }

    private async onAssigningTickets() {

        try {
            console.log( "Method \"AssigningTickets\" has been emitted" );

            const unassignedTickets = await this.dependencies.issueTicketRepository.getUnassignedTickets();

            console.log( `Found ${unassignedTickets?.length} unassigned tickets` );

            if ( !unassignedTickets?.length ) {
                console.log( 'Skipped...' );
                return;
            }

            const agents = await this.dependencies.supportAgentRepository.getFreeAgents();

            console.log( `Found ${agents?.length} free agents` );

            if ( !agents.length ) {
                console.log( 'Skipped...' );
                return;
            }

            await this.assignTicketsToAgents( unassignedTickets, agents );
        } catch ( err ) {
            console.error( err );
        }

    }

    private async assignTicketsToAgents( tickets: IssueTicketEntity[], agents: SupportAgentEntity[]) {

        const pool = [ ...agents ];
console.log(tickets);
        for ( const ticket of tickets ) {

            if ( !pool.length ) {
                console.log( 'There are no more free agents' );
                return;
            }

            ticket.supportAgentId = pool.pop().id;

            await this.updateTicket(ticket);

            console.log( `Support agent #${ticket.supportAgentId} has been assigned to ticket #${ticket.id}` );
        }

    }
}
