import { TicketManagerService } from './ticket-manager.service';
import {
    SupportAgentRepository,
    IssueTicketRepository
} from "../../repository";
import { FindManyOptions } from "typeorm";
import { IssueTicketEntity, SupportAgentEntity } from "../../entity";

const delay = (t = 1000) => new Promise((res) => setTimeout(res, t));

class STUBBDProvider {
    findOne( id: number ) {
        return Promise.resolve( { id } );
    }

    find( options?: FindManyOptions ) {
        return Promise.resolve( [] );
    }

    create( data: Partial<any> ) {
        return Promise.resolve( data );
    }

    update( id: number, patchData: any ) {
        return Promise.resolve( { id, ...patchData } );
    }

    delete( id: number ) {
        return Promise.resolve( {} );
    }
}


describe( 'TicketManagerService', () => {

    const provider = new STUBBDProvider() as any;


    describe( 'TicketManagerService:emitAssigningTickets:AllEmpty', () => {

        const supportAgentRepository = new SupportAgentRepository( provider );
        const issueTicketRepository = new IssueTicketRepository( provider );

        const service = new TicketManagerService( { issueTicketRepository, supportAgentRepository } );


        const spy = jest.spyOn( service, 'updateTicket' );

        service.emitAssigningTickets();

        it( 'should not be called', async () => {
            await delay(1000);
            expect( spy ).not.toBeCalled();
        } );
    } );

    describe( 'TicketManagerService:emitAssigningTickets:OneTicketToOneAgent', () => {

        const supportAgentRepository = new SupportAgentRepository( provider );
        const issueTicketRepository = new IssueTicketRepository( provider );

        const service = new TicketManagerService( { issueTicketRepository, supportAgentRepository } );

        supportAgentRepository.getFreeAgents = jest.fn(
            () => Promise.resolve( [ { id: 1 } ] as SupportAgentEntity[] )
        );
        issueTicketRepository.getUnassignedTickets = jest.fn(
            () => Promise.resolve( [ { id: 1 } ] as IssueTicketEntity[] )
        );

        const spy = jest.spyOn( service, 'updateTicket' );

        service.emitAssigningTickets();

        it( 'should be called once', async () => {
            await delay(1000);
            expect( spy ).toBeCalledTimes(1);
        } );

    } );

    describe( 'TicketManagerService:emitAssigningTickets:ManyTickerToOneAgent',  () => {

        const supportAgentRepository = new SupportAgentRepository( provider );
        const issueTicketRepository = new IssueTicketRepository( provider );

        const service = new TicketManagerService( { issueTicketRepository, supportAgentRepository } );

        supportAgentRepository.getFreeAgents = jest.fn(
            () => Promise.resolve( [ { id: 1 } ] as SupportAgentEntity[] )
        );
        issueTicketRepository.getUnassignedTickets = jest.fn(
            () => Promise.resolve( [ { id: 1 }, { id: 2 }, { id: 3 } ] as IssueTicketEntity[] )
        );

        const spy = jest.spyOn( service, 'updateTicket' );

        service.emitAssigningTickets();

        it( 'should be called once', async () => {
            await delay(1000);
            expect( spy ).toBeCalledTimes(1);
        } );

    } );

    describe( 'TicketManagerService:emitAssigningTickets:ManyTicketsToManyAgents',  () => {

        const supportAgentRepository = new SupportAgentRepository( provider );
        const issueTicketRepository = new IssueTicketRepository( provider );

        const service = new TicketManagerService( { issueTicketRepository, supportAgentRepository } );

        supportAgentRepository.getFreeAgents = jest.fn(
            () => Promise.resolve( [ { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 } ] as SupportAgentEntity[] )
        );
        issueTicketRepository.getUnassignedTickets = jest.fn(
            () => Promise.resolve( [ { id: 1 }, { id: 2 }, { id: 3 } ] as IssueTicketEntity[] )
        );

        const spy = jest.spyOn( service, 'updateTicket' );

        service.emitAssigningTickets();

        it( 'should be called 3 times', async () => {
            await delay(1000);
            expect( spy ).toBeCalledTimes(3);
        } );

    } );

    describe( 'TicketManagerService:emitAssigningTickets:OneTicketsToManyAgents',  () => {

        const supportAgentRepository = new SupportAgentRepository( provider );
        const issueTicketRepository = new IssueTicketRepository( provider );

        const service = new TicketManagerService( { issueTicketRepository, supportAgentRepository } );

        supportAgentRepository.getFreeAgents = jest.fn(
            () => Promise.resolve( [ { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 } ] as SupportAgentEntity[] )
        );
        issueTicketRepository.getUnassignedTickets = jest.fn(
            () => Promise.resolve( [ { id: 1 } ] as IssueTicketEntity[] )
        );

        const spy = jest.spyOn( service, 'updateTicket' );

        service.emitAssigningTickets();

        it( 'should be called 1 times', async () => {
            await delay(1000);
            expect( spy ).toBeCalledTimes(1);
        } );

    } );
} );
