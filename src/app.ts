import Koa from "koa";
import bodyParser from "koa-bodyparser";
import helmet from "koa-helmet";
import cors from "@koa/cors";
import winston from "winston";
import { Connection, createConnection, getManager } from "typeorm";
import "reflect-metadata";

import { logger } from "./middleware";
import createRouter from "./router";
import { IssueTicketRepository, SupportAgentRepository } from "./repository";
import { IssueTicketEntity, SupportAgentEntity } from "./entity";
import { HealthController, IssueTicketController, SupportAgentController } from "./controller";
import { Server } from "http";
import * as Router from "@koa/router";
import { TicketManagerService } from "./service/ticket-manager";


export default class App {
    private server: Server;
    private dbConnection: Connection;

    async start( port: number, isDebugMode: boolean ) {

        this.dbConnection = await this.createDBConnection();

        // Initialize data access layer (DAL) repos
        const supportAgentRepository = new SupportAgentRepository( getManager().getRepository( SupportAgentEntity ) );
        const issueTicketRepository = new IssueTicketRepository( getManager().getRepository( IssueTicketEntity ) );

        const ticketManagerService = new TicketManagerService({
            supportAgentRepository,
            issueTicketRepository
        });

        // Initialized controller with injected dependencies
        const healthController = new HealthController();
        const supportAgentController = new SupportAgentController( {
            supportAgentRepository
        } );
        const issueTicketController = new IssueTicketController( {
            issueTicketRepository
        } );

        // Enable routes
        const router = createRouter( {
            healthController,
            issueTicketController,
            supportAgentController,
            ticketManagerService
        }, isDebugMode);

        this.server = this.initServer(port, router);

        console.log( `Server running on port ${port}` );

        // Process already exist tickets
        ticketManagerService.emitAssigningTickets();
    }

    async terminate(signal: NodeJS.Signals) {
        // by default Docker waits for 10 seconds before killing,
        // make sure you'll close all pending connections within 10 seconds
        console.log(`Received ${signal} signal. Terminating...`);

        // Close db connections
        this.dbConnection.close();

        // Stop HTTP server
        this.server.close();
    }

    private initServer(port: number, router: Router){
        //
        // Initialize framework
        //
        const app = new Koa();

        // Provides important security headers to make your app more secure
        app.use( helmet() );

        // Enable cors with default options
        app.use( cors() );

        // Logger middleware -> use winston as logger (logging.ts with config)
        app.use( logger( winston ) );

        // Enable bodyParser with default options
        app.use( bodyParser() );

        app
            .use( router.routes() )
            .use( router.allowedMethods() );

        return app.listen( port );
    }

    private createDBConnection(){

        // create connection with database
        // note that its not active database connection
        // TypeORM creates you connection pull to uses connections from pull on your requests
        //
        // For understanding how to setup connection credentials
        // pls view this topic : https://typeorm.io/#/using-ormconfig/using-environment-variables
        return createConnection();
    }
}
