import Router from '@koa/router';
import {HealthController, SupportAgentController, IssueTicketController} from './controller';
import {BaseContext} from 'koa';
import {TicketManagerService} from './service/ticket-manager';
import {requestHook} from './middleware/request_hook';


export interface ICreateRouterDependencies {
    healthController: HealthController;
    supportAgentController: SupportAgentController;
    issueTicketController: IssueTicketController;
    ticketManagerService: TicketManagerService;
}

const createWrapper = (isDebugMode: boolean) => async (ctx: BaseContext, promiser: () => Promise<any> | any) => {
    try {
        const data = await promiser();
        ctx.body = {data};
    } catch (error) {

        const statusCode = error.statusCode || 500;

        const isValidationError = statusCode >= 400 && statusCode < 500;

        if (isValidationError || isDebugMode) {
            ctx.status = statusCode;
            ctx.body = {statusCode, error: {message: error.message}};
        } else {
            ctx.status = 500;
            ctx.body = {
                error: {
                    statusCode: 500,
                    message: 'Something went wrong'
                }
            };
        }
    }
};


export default function createRouter(dependencies: ICreateRouterDependencies, isDebugMode = false) {

    /**
     * We don`t pass the request context directly into controller methods to decouple framework from logic layer
     */

    const router = new Router();
    const wrapper = createWrapper(isDebugMode);

    router.get(
        '/ping',
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.healthController.ping())
    );

    /******************** support_agent ********************/

    router.get(
        '/support_agent',
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.supportAgentController.getList())
    );

    router.get(
        '/support_agent/:id',
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.supportAgentController.getOneById(+ctx.params.id))
    );

    router.post(
        '/support_agent',
        requestHook(dependencies.ticketManagerService.emitAssigningTickets),
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.supportAgentController.createOne(ctx.request.body))
    );

    router.delete(
        '/support_agent/:id',
        requestHook(dependencies.ticketManagerService.emitAssigningTickets),
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.supportAgentController.deleteOneById(+ctx.params.id))
    );

    /******************** issue_ticket ********************/

    router.get(
        '/issue_ticket',
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.issueTicketController.getList())
    );

    router.get(
        '/issue_ticket/:id',
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.issueTicketController.getOneById(+ctx.params.id))
    );

    router.patch(
        '/issue_ticket/:id/done',
        requestHook(dependencies.ticketManagerService.emitAssigningTickets),
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.issueTicketController.setStatusDone(+ctx.params.id))
    );

    router.post(
        '/issue_ticket',
        requestHook(dependencies.ticketManagerService.emitAssigningTickets),
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.issueTicketController.createOne(ctx.request.body))
    );

    router.delete(
        '/issue_ticket/:id',
        requestHook(dependencies.ticketManagerService.emitAssigningTickets),
        (ctx: BaseContext) => wrapper(ctx, () => dependencies.issueTicketController.deleteOneById(+ctx.params.id))
    );

    return router;
};
