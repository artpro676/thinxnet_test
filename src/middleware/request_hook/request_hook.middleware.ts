import { BaseContext } from "koa";

const requestHook = (after: () => any) => async (ctx: BaseContext, next: () => Promise<any>) => {

    await next();

    try {
        await after();
    } catch (err) {
        console.error(err);
    }
};

export { requestHook };
