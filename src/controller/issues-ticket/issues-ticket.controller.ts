import autobind from "autobind-decorator";

import { IssueTicketRepository } from "../../repository";
import { IssueTicketEntity, IssueTicketStatusEnum, SupportAgentEntity } from "../../entity";
import { ApiError } from "../../error";

export interface IIssueTicketControllerDependencies {
    issueTicketRepository: IssueTicketRepository;
}

@autobind
export class IssueTicketController {

    private dependencies: IIssueTicketControllerDependencies;

    constructor( dependencies: IIssueTicketControllerDependencies ) {

        if ( !dependencies?.issueTicketRepository ) {
            throw new Error( "Parameter \"issueTicketRepository\" should not be empty" );
        }

        this.dependencies = dependencies;
    }

    public async getList(  ): Promise<IssueTicketEntity[]> {
        return this.dependencies.issueTicketRepository.getList();
    }

    public getOneById(id: number): Promise<IssueTicketEntity> {

        if(!id) {
            throw new ApiError('Invalid parameter id',400);
        }

        const item = this.dependencies.issueTicketRepository.getOneById(id);

        if(!item) {
            throw new ApiError('Not found', 404);
        }

        return item;
    }

    public createOne(data: Partial<SupportAgentEntity>): Promise<IssueTicketEntity> {

        if(!data) {
            // kinda basic validation
            throw new ApiError('Input data should not be empty', 400);
        }

        return this.dependencies.issueTicketRepository.create(data);
    }

    public async deleteOneById(id: number): Promise<any> {

        if(!id) {
            // kinda basic validation
            throw new ApiError('Invalid parameter id',400);
        }

        await this.dependencies.issueTicketRepository.deleteById(id);

        return true;
    }

    public async setStatusDone(id: number): Promise<boolean> {
        if(!id) {
            // kinda basic validation
            throw new ApiError('Invalid parameter id',400);
        }

        const found = await this.dependencies.issueTicketRepository.getOneById(id);

        if(!found) {
            throw new ApiError('Not found', 404);
        }

        await this.dependencies.issueTicketRepository.updateById(id,  {status: IssueTicketStatusEnum.DONE});

        return true;
    }

}
