import autobind from "autobind-decorator";

@autobind
export class HealthController {

    public ping() {
        return "pong";
    }

}
