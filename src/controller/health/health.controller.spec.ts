import { HealthController } from './health.controller';

describe('HealthController', () => {
  const cotroller = new HealthController();

  describe('HealthController:Ping', () => {
    const result = cotroller.ping();

    it('should be "pong"', () => {
      expect(result).toEqual('pong');
    });
  });
});
