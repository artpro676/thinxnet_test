import autobind from "autobind-decorator";

import { SupportAgentRepository } from "../../repository";
import { SupportAgentEntity } from "../../entity";
import { ApiError } from "../../error";

export interface ISupportAgentControllerDependencies {
    supportAgentRepository: SupportAgentRepository;
}

@autobind
export class SupportAgentController {

    private dependencies: ISupportAgentControllerDependencies;

    constructor( dependencies: ISupportAgentControllerDependencies ) {

        if ( !dependencies?.supportAgentRepository ) {
            throw new Error( "Parameter \"supportAgentRepository\" should not be empty" );
        }

        this.dependencies = dependencies;
    }

    public getList(): Promise<any[]> {
        return this.dependencies.supportAgentRepository.getList();
    }

    public getOneById( id: number ): Promise<SupportAgentEntity> {

        if ( !id ) {
            throw new ApiError( 'Invalid parameter id', 400 );
        }

        const item = this.dependencies.supportAgentRepository.getOneById( id );

        if ( !item ) {
            throw new ApiError( 'Not found', 404 );
        }

        return item;
    }

    public createOne( data: Partial<SupportAgentEntity> ): Promise<SupportAgentEntity> {

        if ( !data ) {
            // kinda basic validation
            throw new ApiError( 'Input data should not be empty', 400 );
        }

        return this.dependencies.supportAgentRepository.create( data );
    }

    public deleteOneById( id: number ): Promise<any> {

        if ( !id ) {
            // kinda basic validation
            throw new ApiError( 'Invalid parameter id', 400 );
        }

        return this.dependencies.supportAgentRepository.deleteById( id );
    }
}


