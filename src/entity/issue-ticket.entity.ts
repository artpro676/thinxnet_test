import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { Length, IsEmail } from "class-validator";
import { DB_TABLE_NAME_ENUM } from "./constants";
import { SupportAgentEntity } from "./support-agent.entity";

export enum IssueTicketStatusEnum {
    PENDING="PENDING",
    DONE="DONE"
}

@Entity( { name: DB_TABLE_NAME_ENUM.ISSUE_TICKET } )
export class IssueTicketEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        name: 'support_agent_id'
    })
    supportAgentId: number | null;

    @Column({
        type: "enum",
        enum: IssueTicketStatusEnum,
        default: IssueTicketStatusEnum.PENDING
    })
    status: IssueTicketStatusEnum;

    @Column( {
        length: 250
    } )
    @Length( 10, 250)
    description: string;

    @Column( {
        length: 50
    } )
    @Length( 10, 50)
    from: string;

    @OneToOne(type => SupportAgentEntity)
    @JoinColumn({name: 'support_agent_id'})
    supportAgent: SupportAgentEntity;
}
