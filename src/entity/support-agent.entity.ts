import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany } from "typeorm";
import { Length, IsEmail } from "class-validator";
import { DB_TABLE_NAME_ENUM } from "./constants";
import { IssueTicketEntity } from "./issue-ticket.entity";

@Entity( { name: DB_TABLE_NAME_ENUM.SUPPORT_AGENT } )
export class SupportAgentEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column( {
        length: 80
    } )
    @Length( 10, 80 )
    name: string;

    @Column( {
        length: 100
    } )
    @Length( 10, 100 )
    @IsEmail()
    email: string;

    @OneToMany(type => IssueTicketEntity, issueTicketEntity => issueTicketEntity.supportAgent)
    @JoinColumn()
    tickets: IssueTicketEntity[];

}

