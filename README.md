# ThinxNet Demo App


## Requirements

 - Nodejs@12.*.* 
 - Typescript 
 - Mysql 5.7 


# Getting Started
- Clone the repository
```
git clone git@gitlab.com:artpro676/thinxnet_test.git
```
- Install dependencies
```
cd thinxnet_test
npm install
npm run migration:up
```
- Run the project directly in TS
```
npm run watch-server
```

- Build and run the project in JS
```
npm run build
npm run start
```

- Run  tests
```
npm run test
```

## Getting TypeScript
TypeScript itself is simple to add to any project with `npm`.
```
npm install -D typescript
```

## Setting up the Database - ORM
This API is prepared to work with an SQL database, using [TypeORM](https://github.com/typeorm/typeorm). In this case we are using postgreSQL, and that is why in the package.json 'pg' has been included. If you where to use a different SQL database remember to install the correspondent driver.

The ORM configuration and connection to the database can be specified in the file 'ormconfig.json'. Here is directly in the connection to the database in 'server.ts' file because a environment variable containing databaseUrl is being used to set the connection data. This is prepared for Heroku, which provides a postgres-string-connection as env variable. In local is being mocked with the docker local postgres as can be seen in ".example.env"

It is importante to notice that, when serving the project directly with *.ts files using ts-node,the configuration for the ORM should specify the *.ts files path, but once the project is built (transpiled) and run as plain js, it will be needed to change it accordingly to find the built js files:

```
"entities": [
      "dist/entity/**/*.js"
   ],
   "migrations": [
      "dist/migration/**/*.js"
   ],
   "subscribers": [
      "dist/subscriber/**/*.js"
   ]
```

**NOTE: this is now automatically handled by the NODE_ENV variable too. 


## Running the build

| Npm Script | Description |
| ------------------------- | ------------------------------------------------------------------------------------------------- |
| `start`                   | Does the same as 'npm run serve'. Can be invoked with `npm start`                                 |
| `build`                   | App build. Runs ALL build tasks (`build-ts`, `lint`)                                              |
| `serve`                   | Runs node on `dist/server/server.js` which is the apps entry point                                |
| `watch-server`            | Nodemon, process restarts if crashes. Continuously watches `.ts` files and re-compiles to `.js`   |
| `build-ts`                | Compiles all source `.ts` files to `.js` files in the `dist` folder                               |
| `lint`                    | Runs ESLint check and fix on project files                                                        |
| `test`                    | Execute Jest Unit tests                                                                           |
| `migrate:up`              | Runs DB migrations                                                                                |


## Environment variables
Create a .env file (or just rename the .example.env) containing all the env variables you want to set, dotenv library will take care of setting them. This project is using three variables at the moment:

    * PORT -> port where the server will be started on, Heroku will set this env variable automatically
    * TYPEORM_CONNECTION -> mysql
    * TYPEORM_HOST -> exact db host (localhost)
    * TYPEORM_USERNAME -> Db username
    * TYPEORM_PASSWORD -> DB password
    * TYPEORM_DATABASE -> DB name
    * TYPEORM_PORT -> port, for mysql its usually 3306
    * TYPEORM_SYNCHRONIZE -> should be false
    * TYPEORM_LOGGING -> boolean
    * TYPEORM_ENTITIES -> src/entity/**/*.ts
    * TYPEORM_MIGRATIONS -> src/migration/**/*.ts

| method             | resource             | description                                                                                    |
|:-------------------|:---------------------|:-----------------------------------------------------------------------------------------------|
| `GET`              | `/ping`              | Returns "pong"
| `GET`              | `/issue_ticket`      | returns the collection of issue_ticket present in the DB                                              |
| `GET`              | `/issue_ticket/:id`  | returns the specified id issue_ticket                                                                  |
| `POST`             | `/issue_ticket`      | creates a issue_ticket in the DB (object issue_ticket to be includued in request's body)                       |
| `DELETE`           | `/issue_ticket/:id`  | deletes issue_ticket with id
| `GET`              | `/support_agent`      | returns the collection of support_agent present in the DB                                              |
| `GET`              | `/support_agent/:id`  | returns the specified id support_agent                                                                  |
| `POST`             | `/support_agent`      | creates a support_agent in the DB (object support_agent to be includued in request's body)                       |
| `DELETE`           | `/support_agent/:id`  | deletes support_agent with id

